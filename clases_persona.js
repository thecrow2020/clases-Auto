class Persona {
    
    constructor(nombre, edad, sexo) {

        this.nombre = nombre;
        this.edad = edad;
        this.sexo = sexo;
        this.energia=100
        
    }
    
    mostrar = () => {

        return `NOMBRE: ${this.nombre}, EDAD: ${this.edad}, SEXO ${this.sexo}... bienvenido al juego de las actividades!!! elige una opcion... RECUERDA QUE TU ENERGIA INICIAL ES DE 100 PUNTOS`
        
    }
    
    informe = () => {

        return `Enhorabuena ${this.nombre}, terminaste tu dia con una energia de ${this.energia} puntos, mañana sera otro dia!!!`

    }

    energiaActualizada = () => {

        if (this.energia <= 20) {

            return `TU ENERGIA HASTA EL MOMENTO ES DE ${this.energia} PUNTOS, DEBES DESCANSAR o ALIMENTARTE`

        }else {

            return `TU ENERGIA HASTA EL MOMENTO ES DE ${this.energia} PUNTOS`

        }

    }

    trabajar = (trabajo) => {
    
       let desgaste = Number(trabajo);

        if (this.energia < 60) {

            return `La energia que posees no te permite contitnuar con la accion TRABAJAR, requieres de 60 puntos para poder hacerla, tu energia es de ${this.energia} `

        }else if (this.edad >= 40 ) {

            let porcentaje = desgaste*10/100
            let totalPorc = porcentaje + desgaste

            this.energia = this.energia - totalPorc

            return `${this.nombre},  TRABAJAR demanda 60 PUNTOS DE ENERGIA, ahora tu ENERGIA ES DE ${this.energia} PUNTOS y un desgaste extra de un 10% por ser mayor de 40 años`

        }else if (this.energia >= desgaste || this.energia >= 60) {

            this.energia = this.energia - desgaste

            return `${this.nombre}, TRABAJAR demanda 60 PUNTOS DE ENERGIA, ahora tu ENERGIA ES DE ${this.energia} PUNTOS`

        }

    }

    mantener = (hogar) => {
        
        let desgaste = Number(hogar);

        if (this.energia < 40) {

            return `La energia que posees no te permite contitnuar con la accion MANTENER(HOGAR), requieres de 40 puntos para poder hacerla, tu energia es de ${this.energia} `

        }else if (this.edad >= 40) {

            let porcentaje = desgaste*10/100;
            let totalPorc = porcentaje + desgaste

            this.energia = this.energia - totalPorc;

            return  `${this.nombre},MANTENER demanda 40 PUNTOS DE ENERGIA, ahora tu ENERGIA ES DE ${this.energia} PUNTOS y un desgaste extra de un 10% por ser mayor de 40 años`

        }else if (this.energia >= desgaste || this.energia >= 40) {

            this.energia = this.energia - desgaste;

            return  `${this.nombre},MANTENER demanda 40 PUNTOS DE ENERGIA, ahora tu ENERGIA ES DE ${this.energia} PUNTOS`

        }else if (this.energia < desgaste || this.energia < 40) {

            return `No tienes suficiente energia para hacer esta actividad, tu ENERGIA es de ${this.energia}, necesitas 40 PUNTOS DE ENERGIA PARA REALIZAR ESTA ACCION`

        }

    }
    
    ejercicio = (ejercicio) => {

        let desgaste = Number(ejercicio);

        if (this.energia < 40) {

            return `La energia que posees no te permite contitnuar con la accion EJERCICIO, requieres de 40 puntos para poder hacerla, tu energia es de ${this.energia} `

        }else if (this.edad >= 40) {

            let porcentaje = desgaste*10/100;
            let totalPorc = porcentaje + desgaste

            this.energia = this.energia - totalPorc;

            return  `${this.nombre} ,HACER EJERCICIO demanda 40 PUNTOS DE ENERGIA, ahora tu ENERGIA ES DE ${this.energia} PUNTOS y un desgaste extra de un 10% por ser mayor de 40 años`
            
        }else if (this.energia >= desgaste || this.energia >= 40) {

            this. energia = this.energia - desgaste

            return `${this.nombre} ,HACER EJERCICIO demanda 40 PUNTOS DE ENERGIA, ahora tu ENERGIA ES DE ${this.energia} PUNTOS`

        }else if (this.energia < desgaste || this.energia < 40) {

            return `No tienes suficiente energia para hacer esta actividad, tu ENERGIA es de ${this.energia}, necesitas 40 PUNTOS DE ENERGIA PARA REALIZAR ESTA ACCION`

        }
    }
    
    netflix = () => {
        
        if (this.energia >= 0 || this.energia < 99) {

            this.energia = this.energia;

            return `Esta actividad  seleccionada no altera tu ENERGIA, tu ENERGIA sigue siendo ${this.energia}`

        }
    }
    
    Dormir = (recarga) => {
        
        let sumar = Number(recarga)

        if (this.energia < 100) {
            
            this.energia = this.energia + sumar

            return `RECARGASTE tu ENERGIA ahora tienes ${this.energia} de ENERGIA`

        }else if (this.energia >= 100) {
            
            this.energia = 100;

            return `Tu energia esta llena`

        }

    }

    comidapequeña = (comer) => {

        let recarga = Number(comer);


        if (this.energia >= 100) {

            return `Tu energia esta al maximo`

        }else if (this.energia >=0 || this.energia < 85) {

            this.energia = this.energia + recarga

            return `Una porcion pequeña proporcina un alza de 15 puntos de energia, tu energia es de ${this.energia}`

        }

    }

    comidagrande = (comermas) => {

        let recarga = Number(comermas);

        if (this.energia >= 100) {

            return `Tu energia esta al maximo`

        }else if (this.energia >= 0 || this.energia < 70) {

            this.energia = this.energia + recarga

            return `Una porcion grande proporcina un alza de 30 puntos de energia, tu energia es de ${this.energia}`

        }

    }

}