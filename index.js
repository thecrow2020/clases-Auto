let error= document.getElementById("error");
let mostrarDatos = document.getElementById("bienvenida");
let ok = document.getElementById("ok");
let msj = document.getElementById("msj");
let informes = document.getElementById("informe");
let persona;

function crearPerfil(perfil) {
    
    let nombre = perfil.children[0].value;
    let edad = Number(perfil.children[1].value);
    let hombre = perfil.children[2].children[1];
    let mujer = perfil.children[2].children[3];
    let nombreFinal = null;
    let edadFinal = null;
    let sexoFinal = null;

    if (isNaN(nombre)) {
        nombreFinal = nombre;
    }else {
        error.innerHTML = 'Ingrese un nombre valido'
        return false;
    }

    if (edad >= 20) {
        edadFinal = edad;
    }else {
        error.innerHTML = 'Debes ser mayor de 20 años'
        return false;
    }
    
    if (edad <= 60) {
        edadFinal = edad;
    }else {
        error.innerHTML ='Debes ser menor de 60'
        return false
    }
    
    if (hombre.checked) {
        sexoFinal = "Masculino"
    }

    if (mujer.checked) {
        sexoFinal = "femenino"
    }

    if (nombreFinal && edadFinal && sexoFinal) {
        persona = new Persona (nombreFinal, edadFinal, sexoFinal);
        ok.innerHTML = 'PERFIL CREADO CORRECTAMENTE';
        mostrarDatos.innerHTML = persona.mostrar()
    }else {
        error.innerHTML = 'complete los datos correctamente'
    }
    
}

function EligeActiv() {
    
    if (document.getElementById("1").checked) {
        msj.innerHTML = persona.trabajar(document.getElementById("1").value)
    }else if (document.getElementById("2").checked) {
        msj.innerHTML = persona.mantener(document.getElementById("2").value)
    }else if (document.getElementById("3").checked) {
        msj.innerHTML = persona.netflix(document.getElementById("3").value)
    }else if (document.getElementById("4").checked) {
        msj.innerHTML = persona.ejercicio(document.getElementById("4").value)
    }else if (document.getElementById("5").checked) {
        msj.innerHTML = persona.Dormir(document.getElementById("5").value)
    }else if (document.getElementById("6").checked) {
        msj.innerHTML = persona.comidapequeña(document.getElementById("6").value)
    }else if (document.getElementById("7").checked) {
        msj.innerHTML = persona.comidagrande(document.getElementById("7").value)
    }
    document.getElementById("energia-inicial").innerHTML = persona.energiaActualizada()
}

function informe() {

    informes.innerHTML = persona.informe()

}

function clear() {
    error.innerHTML = "";
    informes.innerHTML = "";
}

function getAll(perfil) {
    crearPerfil(perfil)
    EligeActiv()
    setTimeout(() => {
        clear()
    }, 5000);
}

